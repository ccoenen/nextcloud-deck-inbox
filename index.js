const { connect, disconnect, unreadMessagesIn, markAsRead } = require("./lib/email");
const { createCard } = require("./lib/deck");

const configname = process.argv[2] || "config.js";
const config = require(configname);

const connection = connect(config.imap);

async function begin() {
	const messages = await unreadMessagesIn(config.folder);
	let cardsCreated = 0;
	let messagesRead = 0;

	for(const message of messages) {
		const card = {
			title: message.subject,
			description: `${message.subject}\r\n\r\n${message.text}\r\n(${message.from})`
		};

		await createCard(card, config.nextcloud);
		cardsCreated++;

		await markAsRead(message);
		messagesRead++;
	}
	connection.end();
	console.log("created %d cards and marked %d mails as seen", cardsCreated, messagesRead);
}

connection.once("ready", () => {
	console.log("connection ready");
	try {
		begin();
	} catch (e) {
		console.error(e);
		disconnect();
		process.exit(1);
	}
});

connection.once("end", () => {
	console.log("connection ended");
});
connection.on("error", (err) => {
	console.error("connection error: ", err);
	process.exit(1);
});

connection.connect();
