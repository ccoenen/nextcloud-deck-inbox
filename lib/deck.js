const fetch = require("node-fetch");

module.exports = {
	createCard: async function messageToCard(card, nextcloud) {
		const url = `https://${nextcloud.host}:${nextcloud.port || 443}/index.php/apps/deck/api/v1.0/boards/${nextcloud.deckBoardId}/stacks/${nextcloud.deckStackId}/cards`;
		const headers = {
			"Content-Type": "application/json",
			"Authorization": "Basic " + Buffer.from(`${nextcloud.user}:${nextcloud.password}`).toString("base64")
		};

		return fetch(url, {
			method: "post",
			body:    JSON.stringify(card),
			headers,
		}).then((result) => {
			if (result.status !== 200) return new Error("error while posting new card: " + result.status);
		});
	}
};
