const Imap = require("imap");

let connection;

const FIELD_MAPPING = {
	"HEADER.FIELDS (SUBJECT)": "subject",
	"HEADER.FIELDS (FROM)": "from",
	"TEXT": "text"
};

async function unreadMessageEmitter(connection, path) {
	return new Promise((resolve, reject) => {
		connection.openBox(path, (err) => {
			if (err) return reject(err);
			connection.search(["UNSEEN"], (err, results) => {
				if (err) return reject(err);
				if (results.length === 0) {
					console.log("no unseen messages");
					resolve();
				} else {
					const f = connection.fetch(results, { bodies: Object.keys(FIELD_MAPPING) });
					resolve(f);
				}
			});
		});
	});
}

async function assembleMessage(msg) {
	return new Promise((resolve /*, _reject */) => {
		const assembledMessage = { rawMessage: msg };
		msg.on("body", (stream, info) => {
			// console.log("%s body part: %j //// %j", prefix, info, stream);
			const target = FIELD_MAPPING[info.which];
			assembledMessage[target] = "";
			stream.on("data", (data) => {
				assembledMessage[target] += data.toString("utf-8");
			});
			stream.on("end", () => {
				assembledMessage[target] = assembledMessage[target].trim();
			});
		});

		msg.once("attributes", function(attrs) {
			assembledMessage.uid = attrs.uid;
		});

		msg.once("end", function() {
			resolve(assembledMessage);
		});
	});
}



module.exports = {
	connect: function (imapConfig) {
		connection = new Imap(Object.assign({}, imapConfig, {
			// debug: function (x) {console.log(x);}
		}));
		return connection;
	},

	disconnect: function () {
		connection.end();
	},

	unreadMessagesIn: async function (folder) {
		return new Promise(async (resolve, reject) => {
			const messageBuffer = [];
			console.log("opening %s", folder);

			const emitter = await unreadMessageEmitter(connection, folder);
			if (!emitter) return resolve(messageBuffer);
			emitter.on("message", async (msg /*, seqno */) => {
				const assembledMessage = await assembleMessage(msg);
				messageBuffer.push(assembledMessage);
			});
			emitter.on("error", (err) => {
				reject(`fetch message error: ${err}`);
			});
			emitter.on("end", () => {
				resolve(messageBuffer);
			});
		});
	},

	markAsRead: async function markAsRead(message) {
		return new Promise((resolve, reject) => {
			connection.addFlags(message.uid, "\\Seen", (err) => {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		});
	}
};
