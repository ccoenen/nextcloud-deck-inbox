# Nextcloud Deck Importer

Monitoring a mailbox to create a deck card for every email. This is not an official Nextcloud project.

## Setup

copy `config.js.example` to `config.js` (or whatever name suits you), and fill in all the details.

## Usage

```sh
    node index.js ./config.js
```

This will run once, to run it periodically, put it into your crontab (or similar things)

```sh
crontab -e

# in your editor of choice add a line like this, this would run every five minutes

*/5 * * * * bash -lc 'cd <directory where it is>; /usr/bin/node index.js ./config.js'
```
